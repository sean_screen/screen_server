package com.ruoyi.feiyu.screen.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feiyu.screen.domain.TArea;
import com.ruoyi.feiyu.screen.service.ITAreaService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 区域Controller
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
@RestController
@RequestMapping("/project/area")
public class TAreaController extends BaseController
{
    @Autowired
    private ITAreaService tAreaService;

    /**
     * 查询区域列表
     */
    @PreAuthorize("@ss.hasPermi('project:area:list')")
    @GetMapping("/list")
    public TableDataInfo list(TArea tArea)
    {
        startPage();
        List<TArea> list = tAreaService.selectTAreaList(tArea);
        return getDataTable(list);
    }

    /**
     * 导出区域列表
     */
    @PreAuthorize("@ss.hasPermi('project:area:export')")
    @Log(title = "区域", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TArea tArea)
    {
        List<TArea> list = tAreaService.selectTAreaList(tArea);
        ExcelUtil<TArea> util = new ExcelUtil<TArea>(TArea.class);
        util.exportExcel(response, list, "区域数据");
    }

    /**
     * 获取区域详细信息
     */
    @PreAuthorize("@ss.hasPermi('project:area:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tAreaService.selectTAreaById(id));
    }

    /**
     * 新增区域
     */
    @PreAuthorize("@ss.hasPermi('project:area:add')")
    @Log(title = "区域", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TArea tArea)
    {
        return toAjax(tAreaService.insertTArea(tArea));
    }

    /**
     * 修改区域
     */
    @PreAuthorize("@ss.hasPermi('project:area:edit')")
    @Log(title = "区域", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TArea tArea)
    {
        return toAjax(tAreaService.updateTArea(tArea));
    }

    /**
     * 删除区域
     */
    @PreAuthorize("@ss.hasPermi('project:area:remove')")
    @Log(title = "区域", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tAreaService.deleteTAreaByIds(ids));
    }
}
