package com.ruoyi.feiyu.screen.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feiyu.screen.domain.TEnergyUsage;
import com.ruoyi.feiyu.screen.service.ITEnergyUsageService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 水电数据Controller
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
@RestController
@RequestMapping("/project/energy_usage")
public class TEnergyUsageController extends BaseController
{
    @Autowired
    private ITEnergyUsageService tEnergyUsageService;

    /**
     * 查询水电数据列表
     */
    @PreAuthorize("@ss.hasPermi('project:energy_usage:list')")
    @GetMapping("/list")
    public TableDataInfo list(TEnergyUsage tEnergyUsage)
    {
        startPage();
        List<TEnergyUsage> list = tEnergyUsageService.selectTEnergyUsageList(tEnergyUsage);
        return getDataTable(list);
    }

    /**
     * 导出水电数据列表
     */
    @PreAuthorize("@ss.hasPermi('project:energy_usage:export')")
    @Log(title = "水电数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TEnergyUsage tEnergyUsage)
    {
        List<TEnergyUsage> list = tEnergyUsageService.selectTEnergyUsageList(tEnergyUsage);
        ExcelUtil<TEnergyUsage> util = new ExcelUtil<TEnergyUsage>(TEnergyUsage.class);
        util.exportExcel(response, list, "水电数据数据");
    }

    /**
     * 获取水电数据详细信息
     */
    @PreAuthorize("@ss.hasPermi('project:energy_usage:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tEnergyUsageService.selectTEnergyUsageById(id));
    }

    /**
     * 新增水电数据
     */
    @PreAuthorize("@ss.hasPermi('project:energy_usage:add')")
    @Log(title = "水电数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TEnergyUsage tEnergyUsage)
    {
        return toAjax(tEnergyUsageService.insertTEnergyUsage(tEnergyUsage));
    }

    /**
     * 修改水电数据
     */
    @PreAuthorize("@ss.hasPermi('project:energy_usage:edit')")
    @Log(title = "水电数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TEnergyUsage tEnergyUsage)
    {
        return toAjax(tEnergyUsageService.updateTEnergyUsage(tEnergyUsage));
    }

    /**
     * 删除水电数据
     */
    @PreAuthorize("@ss.hasPermi('project:energy_usage:remove')")
    @Log(title = "水电数据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tEnergyUsageService.deleteTEnergyUsageByIds(ids));
    }
}
