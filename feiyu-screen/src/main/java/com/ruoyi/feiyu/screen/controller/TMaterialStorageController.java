package com.ruoyi.feiyu.screen.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feiyu.screen.domain.TMaterialStorage;
import com.ruoyi.feiyu.screen.service.ITMaterialStorageService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 原料Controller
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
@RestController
@RequestMapping("/project/material_storage")
public class TMaterialStorageController extends BaseController
{
    @Autowired
    private ITMaterialStorageService tMaterialStorageService;

    /**
     * 查询原料列表
     */
    @PreAuthorize("@ss.hasPermi('project:material_storage:list')")
    @GetMapping("/list")
    public TableDataInfo list(TMaterialStorage tMaterialStorage)
    {
        startPage();
        List<TMaterialStorage> list = tMaterialStorageService.selectTMaterialStorageList(tMaterialStorage);
        return getDataTable(list);
    }

    /**
     * 导出原料列表
     */
    @PreAuthorize("@ss.hasPermi('project:material_storage:export')")
    @Log(title = "原料", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TMaterialStorage tMaterialStorage)
    {
        List<TMaterialStorage> list = tMaterialStorageService.selectTMaterialStorageList(tMaterialStorage);
        ExcelUtil<TMaterialStorage> util = new ExcelUtil<TMaterialStorage>(TMaterialStorage.class);
        util.exportExcel(response, list, "原料数据");
    }

    /**
     * 获取原料详细信息
     */
    @PreAuthorize("@ss.hasPermi('project:material_storage:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tMaterialStorageService.selectTMaterialStorageById(id));
    }

    /**
     * 新增原料
     */
    @PreAuthorize("@ss.hasPermi('project:material_storage:add')")
    @Log(title = "原料", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TMaterialStorage tMaterialStorage)
    {
        return toAjax(tMaterialStorageService.insertTMaterialStorage(tMaterialStorage));
    }

    /**
     * 修改原料
     */
    @PreAuthorize("@ss.hasPermi('project:material_storage:edit')")
    @Log(title = "原料", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TMaterialStorage tMaterialStorage)
    {
        return toAjax(tMaterialStorageService.updateTMaterialStorage(tMaterialStorage));
    }

    /**
     * 删除原料
     */
    @PreAuthorize("@ss.hasPermi('project:material_storage:remove')")
    @Log(title = "原料", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tMaterialStorageService.deleteTMaterialStorageByIds(ids));
    }
}
