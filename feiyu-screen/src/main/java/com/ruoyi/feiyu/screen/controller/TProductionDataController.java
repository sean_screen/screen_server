package com.ruoyi.feiyu.screen.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feiyu.screen.domain.TProductionData;
import com.ruoyi.feiyu.screen.service.ITProductionDataService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 生产数据Controller
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
@RestController
@RequestMapping("/project/production_data")
public class TProductionDataController extends BaseController
{
    @Autowired
    private ITProductionDataService tProductionDataService;

    /**
     * 查询生产数据列表
     */
    @PreAuthorize("@ss.hasPermi('project:production_data:list')")
    @GetMapping("/list")
    public TableDataInfo list(TProductionData tProductionData)
    {
        startPage();
        List<TProductionData> list = tProductionDataService.selectTProductionDataList(tProductionData);
        return getDataTable(list);
    }

    /**
     * 导出生产数据列表
     */
    @PreAuthorize("@ss.hasPermi('project:production_data:export')")
    @Log(title = "生产数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TProductionData tProductionData)
    {
        List<TProductionData> list = tProductionDataService.selectTProductionDataList(tProductionData);
        ExcelUtil<TProductionData> util = new ExcelUtil<TProductionData>(TProductionData.class);
        util.exportExcel(response, list, "生产数据数据");
    }

    /**
     * 获取生产数据详细信息
     */
    @PreAuthorize("@ss.hasPermi('project:production_data:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tProductionDataService.selectTProductionDataById(id));
    }

    /**
     * 新增生产数据
     */
    @PreAuthorize("@ss.hasPermi('project:production_data:add')")
    @Log(title = "生产数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TProductionData tProductionData)
    {
        return toAjax(tProductionDataService.insertTProductionData(tProductionData));
    }

    /**
     * 修改生产数据
     */
    @PreAuthorize("@ss.hasPermi('project:production_data:edit')")
    @Log(title = "生产数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TProductionData tProductionData)
    {
        return toAjax(tProductionDataService.updateTProductionData(tProductionData));
    }

    /**
     * 删除生产数据
     */
    @PreAuthorize("@ss.hasPermi('project:production_data:remove')")
    @Log(title = "生产数据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tProductionDataService.deleteTProductionDataByIds(ids));
    }
}
