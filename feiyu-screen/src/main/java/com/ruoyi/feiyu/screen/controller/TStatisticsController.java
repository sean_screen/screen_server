package com.ruoyi.feiyu.screen.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feiyu.screen.domain.TStatistics;
import com.ruoyi.feiyu.screen.service.ITStatisticsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 统计Controller
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
@RestController
@RequestMapping("/project/statistics")
public class TStatisticsController extends BaseController
{
    @Autowired
    private ITStatisticsService tStatisticsService;

    /**
     * 查询统计列表
     */
    @PreAuthorize("@ss.hasPermi('project:statistics:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStatistics tStatistics)
    {
        startPage();
        List<TStatistics> list = tStatisticsService.selectTStatisticsList(tStatistics);
        return getDataTable(list);
    }

    /**
     * 导出统计列表
     */
    @PreAuthorize("@ss.hasPermi('project:statistics:export')")
    @Log(title = "统计", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TStatistics tStatistics)
    {
        List<TStatistics> list = tStatisticsService.selectTStatisticsList(tStatistics);
        ExcelUtil<TStatistics> util = new ExcelUtil<TStatistics>(TStatistics.class);
        util.exportExcel(response, list, "统计数据");
    }

    /**
     * 获取统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('project:statistics:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tStatisticsService.selectTStatisticsById(id));
    }

    /**
     * 新增统计
     */
    @PreAuthorize("@ss.hasPermi('project:statistics:add')")
    @Log(title = "统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStatistics tStatistics)
    {
        return toAjax(tStatisticsService.insertTStatistics(tStatistics));
    }

    /**
     * 修改统计
     */
    @PreAuthorize("@ss.hasPermi('project:statistics:edit')")
    @Log(title = "统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStatistics tStatistics)
    {
        return toAjax(tStatisticsService.updateTStatistics(tStatistics));
    }

    /**
     * 删除统计
     */
    @PreAuthorize("@ss.hasPermi('project:statistics:remove')")
    @Log(title = "统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tStatisticsService.deleteTStatisticsByIds(ids));
    }
}
