package com.ruoyi.feiyu.screen.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.TreeEntity;

/**
 * 区域对象 t_area
 *
 * @author ruoyi
 * @date 2023-10-28
 */
@ApiModel(value = "TArea",description = "区域对象 t_area")
public class TArea extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /** 记录ID */
    @ApiModelProperty(value = "${comment}")
    @TableField(value = "id")
    private Long id;

    /** 区域类型，0 综合 */
    @Excel(name = "区域类型，0 综合")
    @ApiModelProperty(value = "区域类型，0 综合")
    @TableField(value = "type")
    private Integer type;

    /** 区域名称 */
    @Excel(name = "区域名称")
    @ApiModelProperty(value = "区域名称")
    @TableField(value = "name")
    private String name;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    @TableField(value = "dept_id")
    private Long deptId;

    /** 区域地址 */
    @Excel(name = "区域地址")
    @ApiModelProperty(value = "区域地址")
    @TableField(value = "area_address")
    private String areaAddress;

    /** 删除标志（0代表存在 2代表删除） */
    @ApiModelProperty(value = "区域地址")
    @TableField(value = "del_flag")
    private String delFlag;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setType(Integer type)
    {
        this.type = type;
    }

    public Integer getType()
    {
        return type;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setDeptId(Long deptId)
    {
        this.deptId = deptId;
    }

    public Long getDeptId()
    {
        return deptId;
    }
    public void setAreaAddress(String areaAddress)
    {
        this.areaAddress = areaAddress;
    }

    public String getAreaAddress()
    {
        return areaAddress;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("parentId", getParentId())
                .append("type", getType())
                .append("name", getName())
                .append("deptId", getDeptId())
                .append("areaAddress", getAreaAddress())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
