package com.ruoyi.feiyu.screen.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 水电数据对象 t_energy_usage
 *
 * @author ruoyi
 * @date 2023-10-28
 */
@ApiModel(value = "TEnergyUsage",description = "水电数据对象 t_energy_usage")
public class TEnergyUsage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 记录ID */
    @ApiModelProperty(value = "记录ID")
    @TableField(value = "id")
    private Long id;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    @TableField(value = "dept_id")
    private Long deptId;

    /** 区域ID */
    @Excel(name = "区域ID")
    @ApiModelProperty(value = "区域ID")
    @TableField(value = "area_id")
    private Long areaId;

    /** 使用日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "使用日期", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "使用日期")
    @TableField(value = "usage_date")
    private Date usageDate;

    /** 用电量 */
    @Excel(name = "用电量")
    @ApiModelProperty(value = "用电量")
    @TableField(value = "electricity_usage")
    private Long electricityUsage;

    /** 用水量 */
    @Excel(name = "用水量")
    @ApiModelProperty(value = "用水量")
    @TableField(value = "water_usage")
    private Long waterUsage;

    /** 删除标志（0代表存在 2代表删除） */
    @ApiModelProperty(value = "用水量")
    @TableField(value = "del_flag")
    private String delFlag;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setDeptId(Long deptId)
    {
        this.deptId = deptId;
    }

    public Long getDeptId()
    {
        return deptId;
    }
    public void setAreaId(Long areaId)
    {
        this.areaId = areaId;
    }

    public Long getAreaId()
    {
        return areaId;
    }
    public void setUsageDate(Date usageDate)
    {
        this.usageDate = usageDate;
    }

    public Date getUsageDate()
    {
        return usageDate;
    }
    public void setElectricityUsage(Long electricityUsage)
    {
        this.electricityUsage = electricityUsage;
    }

    public Long getElectricityUsage()
    {
        return electricityUsage;
    }
    public void setWaterUsage(Long waterUsage)
    {
        this.waterUsage = waterUsage;
    }

    public Long getWaterUsage()
    {
        return waterUsage;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("deptId", getDeptId())
                .append("areaId", getAreaId())
                .append("usageDate", getUsageDate())
                .append("electricityUsage", getElectricityUsage())
                .append("waterUsage", getWaterUsage())
                .append("remark", getRemark())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
