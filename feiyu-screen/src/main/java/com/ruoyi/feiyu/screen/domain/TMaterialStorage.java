package com.ruoyi.feiyu.screen.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 原料对象 t_material_storage
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
@ApiModel(value = "TMaterialStorage",description = "原料对象 t_material_storage")
public class TMaterialStorage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 记录ID */
    @ApiModelProperty(value = "${comment}")
    @TableField(value = "id")
    private Long id;

    /** 原料名称 */
    @Excel(name = "原料名称")
    @ApiModelProperty(value = "原料名称")
    @TableField(value = "name")
    private String name;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    @TableField(value = "dept_id")
    private Long deptId;

    /** 区域ID */
    @Excel(name = "区域ID")
    @ApiModelProperty(value = "区域ID")
    @TableField(value = "area_id")
    private Long areaId;

    /** 原料数量 */
    @Excel(name = "原料数量")
    @ApiModelProperty(value = "原料数量")
    @TableField(value = "quantity")
    private Long quantity;

    /** 删除标志（0代表存在 2代表删除） */
    @ApiModelProperty(value = "原料数量")
    @TableField(value = "del_flag")
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setAreaId(Long areaId) 
    {
        this.areaId = areaId;
    }

    public Long getAreaId() 
    {
        return areaId;
    }
    public void setQuantity(Long quantity) 
    {
        this.quantity = quantity;
    }

    public Long getQuantity() 
    {
        return quantity;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("deptId", getDeptId())
            .append("areaId", getAreaId())
            .append("quantity", getQuantity())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
