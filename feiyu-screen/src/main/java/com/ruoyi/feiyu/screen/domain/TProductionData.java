package com.ruoyi.feiyu.screen.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 生产数据对象 t_production_data
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
@ApiModel(value = "TProductionData",description = "生产数据对象 t_production_data")
public class TProductionData extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 记录ID */
    @ApiModelProperty(value = "${comment}")
    @TableField(value = "id")
    private Long id;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    @TableField(value = "dept_id")
    private Long deptId;

    /** 生产完成日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "生产完成日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "生产完成日期")
    @TableField(value = "production_date")
    private Date productionDate;

    /** 生产数量 */
    @Excel(name = "生产数量")
    @ApiModelProperty(value = "生产数量")
    @TableField(value = "production_quantity")
    private Long productionQuantity;

    /** 删除标志（0代表存在 2代表删除） */
    @ApiModelProperty(value = "生产数量")
    @TableField(value = "del_flag")
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setProductionDate(Date productionDate) 
    {
        this.productionDate = productionDate;
    }

    public Date getProductionDate() 
    {
        return productionDate;
    }
    public void setProductionQuantity(Long productionQuantity) 
    {
        this.productionQuantity = productionQuantity;
    }

    public Long getProductionQuantity() 
    {
        return productionQuantity;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("deptId", getDeptId())
            .append("productionDate", getProductionDate())
            .append("productionQuantity", getProductionQuantity())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
