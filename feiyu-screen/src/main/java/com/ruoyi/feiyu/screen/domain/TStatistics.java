package com.ruoyi.feiyu.screen.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 统计对象 t_statistics
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
@ApiModel(value = "TStatistics",description = "统计对象 t_statistics")
public class TStatistics extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 记录ID */
    @ApiModelProperty(value = "${comment}")
    @TableField(value = "id")
    private Long id;

    /** 统计数据的日期，是统计周期的开始日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "统计数据的日期，是统计周期的开始日期", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "统计数据的日期，是统计周期的开始日期")
    @TableField(value = "date")
    private Date date;

    /** 区域名称 */
    @Excel(name = "区域名称")
    @ApiModelProperty(value = "区域名称")
    @TableField(value = "area_name")
    private String areaName;

    /** 统计数据的类型，1 报警数量，2 生产数量，3 能源消耗数量 */
    @Excel(name = "统计数据的类型，1 报警数量，2 生产数量，3 能源消耗数量")
    @ApiModelProperty(value = "统计数据的类型，1 报警数量，2 生产数量，3 能源消耗数量")
    @TableField(value = "type")
    private Long type;

    /** 统计数量 */
    @Excel(name = "统计数量")
    @ApiModelProperty(value = "统计数量")
    @TableField(value = "count")
    private Long count;

    /** 统计粒度，1 日，2 周，3 月 */
    @Excel(name = "统计粒度，1 日，2 周，3 月")
    @ApiModelProperty(value = "统计粒度，1 日，2 周，3 月")
    @TableField(value = "period")
    private Long period;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDate(Date date) 
    {
        this.date = date;
    }

    public Date getDate() 
    {
        return date;
    }
    public void setAreaName(String areaName) 
    {
        this.areaName = areaName;
    }

    public String getAreaName() 
    {
        return areaName;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setCount(Long count) 
    {
        this.count = count;
    }

    public Long getCount() 
    {
        return count;
    }
    public void setPeriod(Long period) 
    {
        this.period = period;
    }

    public Long getPeriod() 
    {
        return period;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("date", getDate())
            .append("areaName", getAreaName())
            .append("type", getType())
            .append("count", getCount())
            .append("period", getPeriod())
            .append("createTime", getCreateTime())
            .toString();
    }
}
