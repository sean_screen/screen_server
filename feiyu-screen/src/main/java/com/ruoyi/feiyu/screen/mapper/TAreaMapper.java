package com.ruoyi.feiyu.screen.mapper;

import java.util.List;
import com.ruoyi.feiyu.screen.domain.TArea;

/**
 * 区域Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
public interface TAreaMapper 
{
    /**
     * 查询区域
     * 
     * @param id 区域主键
     * @return 区域
     */
    public TArea selectTAreaById(Long id);

    /**
     * 查询区域列表
     * 
     * @param tArea 区域
     * @return 区域集合
     */
    public List<TArea> selectTAreaList(TArea tArea);

    /**
     * 新增区域
     * 
     * @param tArea 区域
     * @return 结果
     */
    public int insertTArea(TArea tArea);

    /**
     * 修改区域
     * 
     * @param tArea 区域
     * @return 结果
     */
    public int updateTArea(TArea tArea);

    /**
     * 删除区域
     * 
     * @param id 区域主键
     * @return 结果
     */
    public int deleteTAreaById(Long id);

    /**
     * 批量删除区域
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTAreaByIds(Long[] ids);
}
