package com.ruoyi.feiyu.screen.mapper;

import java.util.List;
import com.ruoyi.feiyu.screen.domain.TEnergyUsage;

/**
 * 水电数据Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
public interface TEnergyUsageMapper 
{
    /**
     * 查询水电数据
     * 
     * @param id 水电数据主键
     * @return 水电数据
     */
    public TEnergyUsage selectTEnergyUsageById(Long id);

    /**
     * 查询水电数据列表
     * 
     * @param tEnergyUsage 水电数据
     * @return 水电数据集合
     */
    public List<TEnergyUsage> selectTEnergyUsageList(TEnergyUsage tEnergyUsage);

    /**
     * 新增水电数据
     * 
     * @param tEnergyUsage 水电数据
     * @return 结果
     */
    public int insertTEnergyUsage(TEnergyUsage tEnergyUsage);

    /**
     * 修改水电数据
     * 
     * @param tEnergyUsage 水电数据
     * @return 结果
     */
    public int updateTEnergyUsage(TEnergyUsage tEnergyUsage);

    /**
     * 删除水电数据
     * 
     * @param id 水电数据主键
     * @return 结果
     */
    public int deleteTEnergyUsageById(Long id);

    /**
     * 批量删除水电数据
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTEnergyUsageByIds(Long[] ids);
}
