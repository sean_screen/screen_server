package com.ruoyi.feiyu.screen.mapper;

import java.util.List;
import com.ruoyi.feiyu.screen.domain.TMaterialStorage;

/**
 * 原料Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
public interface TMaterialStorageMapper 
{
    /**
     * 查询原料
     * 
     * @param id 原料主键
     * @return 原料
     */
    public TMaterialStorage selectTMaterialStorageById(Long id);

    /**
     * 查询原料列表
     * 
     * @param tMaterialStorage 原料
     * @return 原料集合
     */
    public List<TMaterialStorage> selectTMaterialStorageList(TMaterialStorage tMaterialStorage);

    /**
     * 新增原料
     * 
     * @param tMaterialStorage 原料
     * @return 结果
     */
    public int insertTMaterialStorage(TMaterialStorage tMaterialStorage);

    /**
     * 修改原料
     * 
     * @param tMaterialStorage 原料
     * @return 结果
     */
    public int updateTMaterialStorage(TMaterialStorage tMaterialStorage);

    /**
     * 删除原料
     * 
     * @param id 原料主键
     * @return 结果
     */
    public int deleteTMaterialStorageById(Long id);

    /**
     * 批量删除原料
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTMaterialStorageByIds(Long[] ids);
}
