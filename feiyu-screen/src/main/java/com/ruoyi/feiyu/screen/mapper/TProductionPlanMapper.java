package com.ruoyi.feiyu.screen.mapper;

import java.util.List;
import com.ruoyi.feiyu.screen.domain.TProductionPlan;

/**
 * 生产计划Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
public interface TProductionPlanMapper 
{
    /**
     * 查询生产计划
     * 
     * @param id 生产计划主键
     * @return 生产计划
     */
    public TProductionPlan selectTProductionPlanById(Long id);

    /**
     * 查询生产计划列表
     * 
     * @param tProductionPlan 生产计划
     * @return 生产计划集合
     */
    public List<TProductionPlan> selectTProductionPlanList(TProductionPlan tProductionPlan);

    /**
     * 新增生产计划
     * 
     * @param tProductionPlan 生产计划
     * @return 结果
     */
    public int insertTProductionPlan(TProductionPlan tProductionPlan);

    /**
     * 修改生产计划
     * 
     * @param tProductionPlan 生产计划
     * @return 结果
     */
    public int updateTProductionPlan(TProductionPlan tProductionPlan);

    /**
     * 删除生产计划
     * 
     * @param id 生产计划主键
     * @return 结果
     */
    public int deleteTProductionPlanById(Long id);

    /**
     * 批量删除生产计划
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTProductionPlanByIds(Long[] ids);
}
