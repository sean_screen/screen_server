package com.ruoyi.feiyu.screen.mapper;

import java.util.List;
import com.ruoyi.feiyu.screen.domain.TStatistics;

/**
 * 统计Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
public interface TStatisticsMapper 
{
    /**
     * 查询统计
     * 
     * @param id 统计主键
     * @return 统计
     */
    public TStatistics selectTStatisticsById(Long id);

    /**
     * 查询统计列表
     * 
     * @param tStatistics 统计
     * @return 统计集合
     */
    public List<TStatistics> selectTStatisticsList(TStatistics tStatistics);

    /**
     * 新增统计
     * 
     * @param tStatistics 统计
     * @return 结果
     */
    public int insertTStatistics(TStatistics tStatistics);

    /**
     * 修改统计
     * 
     * @param tStatistics 统计
     * @return 结果
     */
    public int updateTStatistics(TStatistics tStatistics);

    /**
     * 删除统计
     * 
     * @param id 统计主键
     * @return 结果
     */
    public int deleteTStatisticsById(Long id);

    /**
     * 批量删除统计
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTStatisticsByIds(Long[] ids);
}
