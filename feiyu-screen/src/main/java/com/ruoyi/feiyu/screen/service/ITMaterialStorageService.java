package com.ruoyi.feiyu.screen.service;

import java.util.List;
import com.ruoyi.feiyu.screen.domain.TMaterialStorage;

/**
 * 原料Service接口
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
public interface ITMaterialStorageService 
{
    /**
     * 查询原料
     * 
     * @param id 原料主键
     * @return 原料
     */
    public TMaterialStorage selectTMaterialStorageById(Long id);

    /**
     * 查询原料列表
     * 
     * @param tMaterialStorage 原料
     * @return 原料集合
     */
    public List<TMaterialStorage> selectTMaterialStorageList(TMaterialStorage tMaterialStorage);

    /**
     * 新增原料
     * 
     * @param tMaterialStorage 原料
     * @return 结果
     */
    public int insertTMaterialStorage(TMaterialStorage tMaterialStorage);

    /**
     * 修改原料
     * 
     * @param tMaterialStorage 原料
     * @return 结果
     */
    public int updateTMaterialStorage(TMaterialStorage tMaterialStorage);

    /**
     * 批量删除原料
     * 
     * @param ids 需要删除的原料主键集合
     * @return 结果
     */
    public int deleteTMaterialStorageByIds(Long[] ids);

    /**
     * 删除原料信息
     * 
     * @param id 原料主键
     * @return 结果
     */
    public int deleteTMaterialStorageById(Long id);
}
