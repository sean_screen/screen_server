package com.ruoyi.feiyu.screen.service;

import java.util.List;
import com.ruoyi.feiyu.screen.domain.TProductionData;

/**
 * 生产数据Service接口
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
public interface ITProductionDataService 
{
    /**
     * 查询生产数据
     * 
     * @param id 生产数据主键
     * @return 生产数据
     */
    public TProductionData selectTProductionDataById(Long id);

    /**
     * 查询生产数据列表
     * 
     * @param tProductionData 生产数据
     * @return 生产数据集合
     */
    public List<TProductionData> selectTProductionDataList(TProductionData tProductionData);

    /**
     * 新增生产数据
     * 
     * @param tProductionData 生产数据
     * @return 结果
     */
    public int insertTProductionData(TProductionData tProductionData);

    /**
     * 修改生产数据
     * 
     * @param tProductionData 生产数据
     * @return 结果
     */
    public int updateTProductionData(TProductionData tProductionData);

    /**
     * 批量删除生产数据
     * 
     * @param ids 需要删除的生产数据主键集合
     * @return 结果
     */
    public int deleteTProductionDataByIds(Long[] ids);

    /**
     * 删除生产数据信息
     * 
     * @param id 生产数据主键
     * @return 结果
     */
    public int deleteTProductionDataById(Long id);
}
