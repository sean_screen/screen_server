package com.ruoyi.feiyu.screen.service;

import java.util.List;
import com.ruoyi.feiyu.screen.domain.TStatistics;

/**
 * 统计Service接口
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
public interface ITStatisticsService 
{
    /**
     * 查询统计
     * 
     * @param id 统计主键
     * @return 统计
     */
    public TStatistics selectTStatisticsById(Long id);

    /**
     * 查询统计列表
     * 
     * @param tStatistics 统计
     * @return 统计集合
     */
    public List<TStatistics> selectTStatisticsList(TStatistics tStatistics);

    /**
     * 新增统计
     * 
     * @param tStatistics 统计
     * @return 结果
     */
    public int insertTStatistics(TStatistics tStatistics);

    /**
     * 修改统计
     * 
     * @param tStatistics 统计
     * @return 结果
     */
    public int updateTStatistics(TStatistics tStatistics);

    /**
     * 批量删除统计
     * 
     * @param ids 需要删除的统计主键集合
     * @return 结果
     */
    public int deleteTStatisticsByIds(Long[] ids);

    /**
     * 删除统计信息
     * 
     * @param id 统计主键
     * @return 结果
     */
    public int deleteTStatisticsById(Long id);
}
