package com.ruoyi.feiyu.screen.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feiyu.screen.mapper.TAreaMapper;
import com.ruoyi.feiyu.screen.domain.TArea;
import com.ruoyi.feiyu.screen.service.ITAreaService;

/**
 * 区域Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
@Service
public class TAreaServiceImpl implements ITAreaService 
{
    @Autowired
    private TAreaMapper tAreaMapper;

    /**
     * 查询区域
     * 
     * @param id 区域主键
     * @return 区域
     */
    @Override
    public TArea selectTAreaById(Long id)
    {
        return tAreaMapper.selectTAreaById(id);
    }

    /**
     * 查询区域列表
     * 
     * @param tArea 区域
     * @return 区域
     */
    @Override
    public List<TArea> selectTAreaList(TArea tArea)
    {
        return tAreaMapper.selectTAreaList(tArea);
    }

    /**
     * 新增区域
     * 
     * @param tArea 区域
     * @return 结果
     */
    @Override
    public int insertTArea(TArea tArea)
    {
        tArea.setCreateBy(""+SecurityUtils.getUserId());
        tArea.setCreateTime(DateUtils.getNowDate());
        return tAreaMapper.insertTArea(tArea);
    }

    /**
     * 修改区域
     * 
     * @param tArea 区域
     * @return 结果
     */
    @Override
    public int updateTArea(TArea tArea)
    {
        tArea.setUpdateTime(DateUtils.getNowDate());
        return tAreaMapper.updateTArea(tArea);
    }

    /**
     * 批量删除区域
     * 
     * @param ids 需要删除的区域主键
     * @return 结果
     */
    @Override
    public int deleteTAreaByIds(Long[] ids)
    {
        return tAreaMapper.deleteTAreaByIds(ids);
    }

    /**
     * 删除区域信息
     * 
     * @param id 区域主键
     * @return 结果
     */
    @Override
    public int deleteTAreaById(Long id)
    {
        return tAreaMapper.deleteTAreaById(id);
    }
}
