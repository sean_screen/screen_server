package com.ruoyi.feiyu.screen.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feiyu.screen.mapper.TEnergyUsageMapper;
import com.ruoyi.feiyu.screen.domain.TEnergyUsage;
import com.ruoyi.feiyu.screen.service.ITEnergyUsageService;

/**
 * 水电数据Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
@Service
public class TEnergyUsageServiceImpl implements ITEnergyUsageService 
{
    @Autowired
    private TEnergyUsageMapper tEnergyUsageMapper;

    /**
     * 查询水电数据
     * 
     * @param id 水电数据主键
     * @return 水电数据
     */
    @Override
    public TEnergyUsage selectTEnergyUsageById(Long id)
    {
        return tEnergyUsageMapper.selectTEnergyUsageById(id);
    }

    /**
     * 查询水电数据列表
     * 
     * @param tEnergyUsage 水电数据
     * @return 水电数据
     */
    @Override
    public List<TEnergyUsage> selectTEnergyUsageList(TEnergyUsage tEnergyUsage)
    {
        return tEnergyUsageMapper.selectTEnergyUsageList(tEnergyUsage);
    }

    /**
     * 新增水电数据
     * 
     * @param tEnergyUsage 水电数据
     * @return 结果
     */
    @Override
    public int insertTEnergyUsage(TEnergyUsage tEnergyUsage)
    {
        tEnergyUsage.setCreateBy(""+ SecurityUtils.getUserId());
        tEnergyUsage.setCreateTime(DateUtils.getNowDate());
        return tEnergyUsageMapper.insertTEnergyUsage(tEnergyUsage);
    }

    /**
     * 修改水电数据
     * 
     * @param tEnergyUsage 水电数据
     * @return 结果
     */
    @Override
    public int updateTEnergyUsage(TEnergyUsage tEnergyUsage)
    {
        tEnergyUsage.setUpdateTime(DateUtils.getNowDate());
        return tEnergyUsageMapper.updateTEnergyUsage(tEnergyUsage);
    }

    /**
     * 批量删除水电数据
     * 
     * @param ids 需要删除的水电数据主键
     * @return 结果
     */
    @Override
    public int deleteTEnergyUsageByIds(Long[] ids)
    {
        return tEnergyUsageMapper.deleteTEnergyUsageByIds(ids);
    }

    /**
     * 删除水电数据信息
     * 
     * @param id 水电数据主键
     * @return 结果
     */
    @Override
    public int deleteTEnergyUsageById(Long id)
    {
        return tEnergyUsageMapper.deleteTEnergyUsageById(id);
    }
}
