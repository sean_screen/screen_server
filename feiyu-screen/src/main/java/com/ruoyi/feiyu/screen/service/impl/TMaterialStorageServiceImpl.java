package com.ruoyi.feiyu.screen.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feiyu.screen.mapper.TMaterialStorageMapper;
import com.ruoyi.feiyu.screen.domain.TMaterialStorage;
import com.ruoyi.feiyu.screen.service.ITMaterialStorageService;

/**
 * 原料Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
@Service
public class TMaterialStorageServiceImpl implements ITMaterialStorageService 
{
    @Autowired
    private TMaterialStorageMapper tMaterialStorageMapper;

    /**
     * 查询原料
     * 
     * @param id 原料主键
     * @return 原料
     */
    @Override
    public TMaterialStorage selectTMaterialStorageById(Long id)
    {
        return tMaterialStorageMapper.selectTMaterialStorageById(id);
    }

    /**
     * 查询原料列表
     * 
     * @param tMaterialStorage 原料
     * @return 原料
     */
    @Override
    public List<TMaterialStorage> selectTMaterialStorageList(TMaterialStorage tMaterialStorage)
    {
        return tMaterialStorageMapper.selectTMaterialStorageList(tMaterialStorage);
    }

    /**
     * 新增原料
     * 
     * @param tMaterialStorage 原料
     * @return 结果
     */
    @Override
    public int insertTMaterialStorage(TMaterialStorage tMaterialStorage)
    {
        tMaterialStorage.setCreateBy(""+ SecurityUtils.getUserId());
        tMaterialStorage.setCreateTime(DateUtils.getNowDate());
        return tMaterialStorageMapper.insertTMaterialStorage(tMaterialStorage);
    }

    /**
     * 修改原料
     * 
     * @param tMaterialStorage 原料
     * @return 结果
     */
    @Override
    public int updateTMaterialStorage(TMaterialStorage tMaterialStorage)
    {
        tMaterialStorage.setUpdateTime(DateUtils.getNowDate());
        return tMaterialStorageMapper.updateTMaterialStorage(tMaterialStorage);
    }

    /**
     * 批量删除原料
     * 
     * @param ids 需要删除的原料主键
     * @return 结果
     */
    @Override
    public int deleteTMaterialStorageByIds(Long[] ids)
    {
        return tMaterialStorageMapper.deleteTMaterialStorageByIds(ids);
    }

    /**
     * 删除原料信息
     * 
     * @param id 原料主键
     * @return 结果
     */
    @Override
    public int deleteTMaterialStorageById(Long id)
    {
        return tMaterialStorageMapper.deleteTMaterialStorageById(id);
    }
}
