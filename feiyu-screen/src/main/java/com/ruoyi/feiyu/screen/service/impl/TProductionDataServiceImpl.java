package com.ruoyi.feiyu.screen.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feiyu.screen.mapper.TProductionDataMapper;
import com.ruoyi.feiyu.screen.domain.TProductionData;
import com.ruoyi.feiyu.screen.service.ITProductionDataService;

/**
 * 生产数据Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
@Service
public class TProductionDataServiceImpl implements ITProductionDataService 
{
    @Autowired
    private TProductionDataMapper tProductionDataMapper;

    /**
     * 查询生产数据
     * 
     * @param id 生产数据主键
     * @return 生产数据
     */
    @Override
    public TProductionData selectTProductionDataById(Long id)
    {
        return tProductionDataMapper.selectTProductionDataById(id);
    }

    /**
     * 查询生产数据列表
     * 
     * @param tProductionData 生产数据
     * @return 生产数据
     */
    @Override
    public List<TProductionData> selectTProductionDataList(TProductionData tProductionData)
    {
        return tProductionDataMapper.selectTProductionDataList(tProductionData);
    }

    /**
     * 新增生产数据
     * 
     * @param tProductionData 生产数据
     * @return 结果
     */
    @Override
    public int insertTProductionData(TProductionData tProductionData)
    {
        tProductionData.setCreateBy(""+ SecurityUtils.getUserId());
        tProductionData.setCreateTime(DateUtils.getNowDate());
        return tProductionDataMapper.insertTProductionData(tProductionData);
    }

    /**
     * 修改生产数据
     * 
     * @param tProductionData 生产数据
     * @return 结果
     */
    @Override
    public int updateTProductionData(TProductionData tProductionData)
    {
        tProductionData.setUpdateTime(DateUtils.getNowDate());
        return tProductionDataMapper.updateTProductionData(tProductionData);
    }

    /**
     * 批量删除生产数据
     * 
     * @param ids 需要删除的生产数据主键
     * @return 结果
     */
    @Override
    public int deleteTProductionDataByIds(Long[] ids)
    {
        return tProductionDataMapper.deleteTProductionDataByIds(ids);
    }

    /**
     * 删除生产数据信息
     * 
     * @param id 生产数据主键
     * @return 结果
     */
    @Override
    public int deleteTProductionDataById(Long id)
    {
        return tProductionDataMapper.deleteTProductionDataById(id);
    }
}
