package com.ruoyi.feiyu.screen.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feiyu.screen.mapper.TProductionPlanMapper;
import com.ruoyi.feiyu.screen.domain.TProductionPlan;
import com.ruoyi.feiyu.screen.service.ITProductionPlanService;

/**
 * 生产计划Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
@Service
public class TProductionPlanServiceImpl implements ITProductionPlanService 
{
    @Autowired
    private TProductionPlanMapper tProductionPlanMapper;

    /**
     * 查询生产计划
     * 
     * @param id 生产计划主键
     * @return 生产计划
     */
    @Override
    public TProductionPlan selectTProductionPlanById(Long id)
    {
        return tProductionPlanMapper.selectTProductionPlanById(id);
    }

    /**
     * 查询生产计划列表
     * 
     * @param tProductionPlan 生产计划
     * @return 生产计划
     */
    @Override
    public List<TProductionPlan> selectTProductionPlanList(TProductionPlan tProductionPlan)
    {
        return tProductionPlanMapper.selectTProductionPlanList(tProductionPlan);
    }

    /**
     * 新增生产计划
     * 
     * @param tProductionPlan 生产计划
     * @return 结果
     */
    @Override
    public int insertTProductionPlan(TProductionPlan tProductionPlan)
    {
        tProductionPlan.setCreateTime(DateUtils.getNowDate());
        return tProductionPlanMapper.insertTProductionPlan(tProductionPlan);
    }

    /**
     * 修改生产计划
     * 
     * @param tProductionPlan 生产计划
     * @return 结果
     */
    @Override
    public int updateTProductionPlan(TProductionPlan tProductionPlan)
    {
        tProductionPlan.setCreateBy(""+ SecurityUtils.getUserId());
        tProductionPlan.setUpdateTime(DateUtils.getNowDate());
        return tProductionPlanMapper.updateTProductionPlan(tProductionPlan);
    }

    /**
     * 批量删除生产计划
     * 
     * @param ids 需要删除的生产计划主键
     * @return 结果
     */
    @Override
    public int deleteTProductionPlanByIds(Long[] ids)
    {
        return tProductionPlanMapper.deleteTProductionPlanByIds(ids);
    }

    /**
     * 删除生产计划信息
     * 
     * @param id 生产计划主键
     * @return 结果
     */
    @Override
    public int deleteTProductionPlanById(Long id)
    {
        return tProductionPlanMapper.deleteTProductionPlanById(id);
    }
}
