package com.ruoyi.feiyu.screen.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feiyu.screen.mapper.TStatisticsMapper;
import com.ruoyi.feiyu.screen.domain.TStatistics;
import com.ruoyi.feiyu.screen.service.ITStatisticsService;

/**
 * 统计Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-28
 */
@Service
public class TStatisticsServiceImpl implements ITStatisticsService 
{
    @Autowired
    private TStatisticsMapper tStatisticsMapper;

    /**
     * 查询统计
     * 
     * @param id 统计主键
     * @return 统计
     */
    @Override
    public TStatistics selectTStatisticsById(Long id)
    {
        return tStatisticsMapper.selectTStatisticsById(id);
    }

    /**
     * 查询统计列表
     * 
     * @param tStatistics 统计
     * @return 统计
     */
    @Override
    public List<TStatistics> selectTStatisticsList(TStatistics tStatistics)
    {
        return tStatisticsMapper.selectTStatisticsList(tStatistics);
    }

    /**
     * 新增统计
     * 
     * @param tStatistics 统计
     * @return 结果
     */
    @Override
    public int insertTStatistics(TStatistics tStatistics)
    {
        tStatistics.setCreateBy(""+ SecurityUtils.getUserId());
        tStatistics.setCreateTime(DateUtils.getNowDate());
        return tStatisticsMapper.insertTStatistics(tStatistics);
    }

    /**
     * 修改统计
     * 
     * @param tStatistics 统计
     * @return 结果
     */
    @Override
    public int updateTStatistics(TStatistics tStatistics)
    {
        return tStatisticsMapper.updateTStatistics(tStatistics);
    }

    /**
     * 批量删除统计
     * 
     * @param ids 需要删除的统计主键
     * @return 结果
     */
    @Override
    public int deleteTStatisticsByIds(Long[] ids)
    {
        return tStatisticsMapper.deleteTStatisticsByIds(ids);
    }

    /**
     * 删除统计信息
     * 
     * @param id 统计主键
     * @return 结果
     */
    @Override
    public int deleteTStatisticsById(Long id)
    {
        return tStatisticsMapper.deleteTStatisticsById(id);
    }
}
