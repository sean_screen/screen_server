insert into sys_dict_type values(101, '区域类型', 'feiyu_area_type',   '0', 'admin', sysdate(), '', null, '登录状态列表');

insert into sys_dict_data values(101, 1,  '综合',     '0',       'feiyu_area_type',   '',   'primary', 'N', '0', 'admin', sysdate(), '', null, '综合区域');

insert into sys_dict_type values(102, '统计力度', 'feiyu_statistics_period',   '0', 'admin', sysdate(), '', null, '登录状态列表');

insert into sys_dict_data values(201, 1,  '日',     '1',       'feiyu_statistics_period',   '',   'primary', 'N', '0', 'admin', sysdate(), '', null, '按日统计');
insert into sys_dict_data values(202, 2,  '周',     '2',       'feiyu_statistics_period',   '',   'primary', 'N', '0', 'admin', sysdate(), '', null, '按周统计');
insert into sys_dict_data values(203, 3,  '月',     '3',       'feiyu_statistics_period',   '',   'primary', 'N', '0', 'admin', sysdate(), '', null, '按月统计');

insert into sys_dict_type values(103, '统计类型', 'feiyu_statistics_type',   '0', 'admin', sysdate(), '', null, '统计类型列表');

insert into sys_dict_data values(301, 1,  '报警数量',     '1',       'feiyu_statistics_type',   '',   'primary', 'N', '0', 'admin', sysdate(), '', null, '报警数量');
insert into sys_dict_data values(302, 2,  '生产数量',     '2',       'feiyu_statistics_type',   '',   'primary', 'N', '0', 'admin', sysdate(), '', null, '生产数量');
insert into sys_dict_data values(303, 3,  '能源消耗',     '3',       'feiyu_statistics_type',   '',   'primary', 'N', '0', 'admin', sysdate(), '', null, '能源消耗');

