insert into sys_menu values('10', '基础信息', '0', '0', 'example',           null, '', 1, 0, 'M', '0', '0', '', 'feiyu',   'admin', sysdate(), '', null, '基础信息管理目录');

-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('区域', '10', '1', 'area', 'project/area/index', 1, 0, 'C', '0', '0', 'project:area:list', '#', 'admin', sysdate(), '', null, '区域菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('区域查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'project:area:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('区域新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'project:area:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('区域修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'project:area:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('区域删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'project:area:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('区域导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'project:area:export',       '#', 'admin', sysdate(), '', null, '');

-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('原料', '10', '1', 'material_storage', 'project/material_storage/index', 1, 0, 'C', '0', '0', 'project:material_storage:list', '#', 'admin', sysdate(), '', null, '原料菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('原料查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'project:material_storage:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('原料新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'project:material_storage:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('原料修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'project:material_storage:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('原料删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'project:material_storage:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('原料导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'project:material_storage:export',       '#', 'admin', sysdate(), '', null, '');

-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('水电数据', '10', '1', 'energy_usage', 'project/energy_usage/index', 1, 0, 'C', '0', '0', 'project:energy_usage:list', '#', 'admin', sysdate(), '', null, '水电数据菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('水电数据查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'project:energy_usage:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('水电数据新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'project:energy_usage:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('水电数据修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'project:energy_usage:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('水电数据删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'project:energy_usage:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('水电数据导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'project:energy_usage:export',       '#', 'admin', sysdate(), '', null, '');


-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('统计', '10', '1', 'statistics', 'project/statistics/index', 1, 0, 'C', '0', '0', 'project:statistics:list', '#', 'admin', sysdate(), '', null, '统计菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('统计查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'project:statistics:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('统计新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'project:statistics:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('统计修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'project:statistics:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('统计删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'project:statistics:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('统计导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'project:statistics:export',       '#', 'admin', sysdate(), '', null, '');

-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('生产数据', '10', '1', 'production_data', 'project/production_data/index', 1, 0, 'C', '0', '0', 'project:production_data:list', '#', 'admin', sysdate(), '', null, '生产数据菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('生产数据查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'project:production_data:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('生产数据新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'project:production_data:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('生产数据修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'project:production_data:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('生产数据删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'project:production_data:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('生产数据导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'project:production_data:export',       '#', 'admin', sysdate(), '', null, '');

-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('生产计划', '10', '1', 'production_plan', 'project/production_plan/index', 1, 0, 'C', '0', '0', 'project:production_plan:list', '#', 'admin', sysdate(), '', null, '生产计划菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('生产计划查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'project:production_plan:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('生产计划新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'project:production_plan:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('生产计划修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'project:production_plan:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('生产计划删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'project:production_plan:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('生产计划导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'project:production_plan:export',       '#', 'admin', sysdate(), '', null, '');

