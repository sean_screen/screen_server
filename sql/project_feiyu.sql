SET FOREIGN_KEY_CHECKS = 0;

-- 区域表
drop table if exists t_area;
CREATE TABLE t_area (
    `id` bigint(20) NOT NULL AUTO_INCREMENT         comment '记录ID',
    `parent_id` bigint(20) NULL                     comment '上级区域ID',
    `type` int NOT NULL                             comment '区域类型，0 综合',
    `name` VARCHAR(255) NOT NULL                    comment '区域名称',
    `dept_id` bigint(20) NOT NULL                   comment '部门ID',
    `area_address` VARCHAR(255) NULL                comment '区域地址',
    `del_flag` char(1)  default '0'                 comment '删除标志（0代表存在 2代表删除）',
    `create_by` varchar(64) NOT NULL                comment '创建者',
    `create_time` datetime NULL DEFAULT now()       comment '创建时间',
    `update_by` varchar(64) NULL                    comment '更新者',
    `update_time` datetime NULL                     comment '更新时间',
    PRIMARY KEY (`id`)
) engine=innodb auto_increment=100 comment = '区域表';
create index t_area_idx1 on t_area(dept_id);

-- 生产计划表
drop table if exists t_production_plan;
CREATE TABLE t_production_plan (
    `id` bigint(20) NOT NULL AUTO_INCREMENT         comment '记录ID',
    `name` VARCHAR(255) NOT NULL                    comment '计划名称',
    `dept_id` bigint(20) NOT NULL                   comment '部门ID',
    `start_time` DATETIME NULL                      comment '计划开始时间',
    `end_time` DATETIME NULL                        comment '计划结束时间',
    `production_quantity` INT NOT NULL              comment '生产数量',
    `del_flag` char(1)  default '0'                 comment '删除标志（0代表存在 2代表删除）',
    `create_by` varchar(64) NOT NULL                comment '创建者',
    `create_time` datetime NULL DEFAULT now()       comment '创建时间',
    `update_by` varchar(64) NULL                    comment '更新者',
    `update_time` datetime NULL                     comment '更新时间',
    PRIMARY KEY (`id`)
) engine=innodb auto_increment=100 comment = '生产计划表';
create index t_production_plan_idx1 on t_production_plan(dept_id);

-- 生产数据表
drop table if exists t_production_data;
CREATE TABLE t_production_data (
    `id` bigint(20) not null AUTO_INCREMENT         comment '记录ID',
    `dept_id` bigint(20) default null               comment '部门ID',
    `production_date` DATE NOT NULL                 comment '生产完成日期',
    `production_quantity` INT NOT NULL              comment '生产数量',
    `remark`  varchar(500) default null             comment '备注',
    `del_flag` char(1) default '0'                  comment '删除标志（0代表存在 2代表删除）',
    `create_by` varchar(64) not null default ''     comment '创建者',
    `create_time` datetime not null default now()   comment '创建时间',
    `update_by` varchar(64) null                    comment '更新者',
    `update_time` datetime null                     comment '更新时间',
    PRIMARY KEY (id)
)engine=innodb auto_increment=200 comment = '生产数据表';
create index t_production_data_idx1 on t_production_data(dept_id);

-- 水电数据表
drop table if exists t_energy_usage;
CREATE TABLE t_energy_usage (
    `id` bigint(20) not null AUTO_INCREMENT         comment '记录ID',
    `dept_id` bigint(20) default null               comment '部门ID',
    `area_id` bigint(20) NOT NULL                   comment '区域ID',
    `usage_date` DATE NOT NULL                      comment '日期',
    `electricity_usage` INT NOT NULL                comment '用电量',
    `water_usage` INT NOT NULL                      comment '用水量',
    `remark`  varchar(500) default null             comment '备注',
    `del_flag` char(1) default '0'                  comment '删除标志（0代表存在 2代表删除）',
    `create_by` varchar(64) not null default ''     comment '创建者',
    `create_time` datetime not null default now()   comment '创建时间',
    `update_by` varchar(64) null                    comment '更新者',
    `update_time` datetime null                     comment '更新时间',
    PRIMARY KEY (id),
    FOREIGN KEY (`area_id`) REFERENCES `t_area`(`id`)
)engine=innodb auto_increment=200 comment = '水电数据表';
create index t_energy_usage_idx1 on t_energy_usage(dept_id);

-- 原料表
drop table if exists t_material_storage;
CREATE TABLE t_material_storage (
    `id` bigint(20) NOT NULL AUTO_INCREMENT         comment '记录ID',
    `name` VARCHAR(255) NOT NULL                    comment '原料名称',
    `dept_id` bigint(20) NOT NULL                   comment '部门ID',
    `area_id` bigint(20) NOT NULL                   comment '区域ID',
    `quantity` INT NOT NULL                         comment '原料数量',
    `del_flag` char(1)  default '0'                 comment '删除标志（0代表存在 2代表删除）',
    `create_by` varchar(64) NOT NULL                comment '创建者',
    `create_time` datetime NULL DEFAULT now()       comment '创建时间',
    `update_by` varchar(64) NULL                    comment '更新者',
    `update_time` datetime NULL                     comment '更新时间',
    PRIMARY KEY (`id`),
    FOREIGN KEY (`area_id`) REFERENCES `t_area`(`id`)
) engine=innodb auto_increment=100 comment = '原料表';
create index t_material_storage_idx1 on t_material_storage(dept_id);

-- 统计表
drop table if exists t_statistics;
CREATE TABLE t_statistics (
    `id` bigint(20) NOT NULL AUTO_INCREMENT         comment '记录ID',
    `date` DATE NOT NULL                            comment '统计数据的日期，是统计周期的开始日期',
    `area_name` VARCHAR(255) NOT NULL               comment '区域名称',
    `type` INT NOT NULL                             comment '统计数据的类型，1 报警数量，2 生产数量，3 能源消耗数量',
    `count` INT NOT NULL                            comment '统计数量',
    `period` int NOT NULL                           comment '统计粒度，1 日，2 周，3 月',
    `create_time` datetime NULL DEFAULT now()       comment '创建时间',
    PRIMARY KEY (`id`)
)engine=innodb auto_increment=100 comment = '统计表';
create index t_statistics_idx1 on t_statistics(date);

SET FOREIGN_KEY_CHECKS = 1;